import  en from "./en"

const nl : typeof en = {
    welcome: 'Schrijf een korte welkomsttekst',
    welcome_paragraph: 'Schrijf een welkomstparagraaf',
    copyright: '©2023 [SiteNaam]. Alle rechten voorbehouden.',
}

export default nl