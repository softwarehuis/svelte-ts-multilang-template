// original, is used as type to check all other language files
export default  {
    welcome: 'Write a welcome text',
    welcome_paragraph: 'Write a descriptive welcome paragraph here',
    copyright: '©2023 [SiteName]. All Rights Reserved.',
}