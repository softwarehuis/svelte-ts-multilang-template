import en_ from "./_en.svelte";
import nl_ from "./_nl.svelte";
//import de_ from "./_de.svelte";
//import es_ from "./_es.svelte";
//import fr_ from "./_fr.svelte";
const en = en_;
const nl = nl_;
//export const de = de_;
//export const es = es_;
//export const fr = fr_;

export const langs: { [key: string]: any } = {
    en: en,
    nl: nl,
    //de: de,
    //es: es,
    //fr: fr,
};